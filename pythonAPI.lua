local function splitLines( text )
    local t={} ; i=1
    for str in string.gmatch(text, "([^".."\n".."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end
local function split(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end
local function randName(len)
    y=""
    for i=0,len do
        y=y..math.random(1,9)
    end
    return y
end
local function pullShellDat(com)
	local name = system.pathForFile( nil, system.ApplicationSupportDirectory):gsub(" "," ").."/"..randName(40)..".txt"
	local executeCommand = os.execute(com.."  > \""..name:gsub(" "," ").."\"")
	print(executeCommand)
	if executeCommand ~= 0 then
		os.remove(name)
		return false,executeCommand
	end
	local file = io.open(name,"r")
	local outp = splitLines(file:read("*all"))
	file:close()
	os.remove(name)
	return outp
end
local function argCompile( tab )
	local outp=""
	if #tab == 0 then
		return outp
	end
	for k,v in ipairs(tab) do
		outp=outp.."\""..v.."\" "
	end
	return outp
end
py={}
local c
py.ver,c=pullShellDat("python --version")
print(py.ver)
if c~=0 then
	error("ERROR:pythonAPI:sorry but i require the command \"python\" defined to work")
end
c=nil
py.ver=split(py.ver," ")[2]
function py.newInstance()
	local inst = {}
	inst.isDone=true
	inst.output={}
	function inst.run( script,args,dir )
		self.isDone=false
		local path =(dir or system.pathForFile( nil, system.ResourceDirectory ))
		local x,code = pullShellDat("python \""..path:gsub(" ","\\ ").."/"..script.."\" "..argCompile(args))
		if not x then
			error("ERROR:failed to execute script python exit code")
		end
		self.output=x
		self.isDone=true
	end
	return inst
end
return py